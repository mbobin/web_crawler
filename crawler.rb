require 'rubygems'
require 'nokogiri'
require 'open-uri'

DATA_DIR = "lyrics"
Dir.mkdir(DATA_DIR) unless File.exists?(DATA_DIR)

BASE_MLDB_URL = "http://www.mldb.org"

continue = true
number = 60
letter_contor = 4
letters =  ('A'..'Z').to_a

while continue == true
  page_url = "#{BASE_MLDB_URL}/aza-#{letters[letter_contor]}-#{number}.html"
  page = Nokogiri::HTML(open(page_url))
  puts "[#{Time.now}] opened #{page_url}"
  rows = page.css('table#thelist tr')
  
  if rows.any?
    rows.each do |row|

      hrefs = row.css("td a").map{ |a| 
        a['href'] if a['href'] =~ /artist/ 
      }.compact.uniq
  
      hrefs.each do |href|
        artist_page_url = "#{BASE_MLDB_URL}/#{href}"
        
        artist_page = Nokogiri::HTML(open(artist_page_url))
        puts "[#{Time.now}] opened #{artist_page_url}"
        song_rows = artist_page.css('table#thelist tr')

        song_rows.each do |song_row|
          song_hrefs = song_row.css("td a").map{ |a| 
            a['href'] if a['href'] =~ /song/ 
          }.compact.uniq

          song_hrefs.each do |song_href|
            lyrics_url = "#{BASE_MLDB_URL}/#{song_href}"
            lyrics_page = Nokogiri::HTML(open(lyrics_url))
            puts "[#{Time.now}] opened #{lyrics_url}"
            song_title = lyrics_page.css('.centerplane > table:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > h1:nth-child(1)').text.chomp[0..-8].encode(:xml => :attr)
            band_title = lyrics_page.css('#thelist > tr:nth-child(2) > td:nth-child(2) > a:nth-child(1)').text.chomp.encode(:xml => :attr)
            album_name = lyrics_page.css('#thelist > tr:nth-child(3) > td:nth-child(2) > a:nth-child(1)').text.chomp.encode(:xml => :attr)
            song_lyrics = lyrics_page.css('.songtext').text.chomp.encode(:xml => :attr)
            local_fname = "#{DATA_DIR}/" + "#{band_title}-#{song_title}".downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '') + ".xml"
            file_content = "<batch><add id='#{local_fname}'><field name='artist'>#{band_title}</field>\n<field name='album'>#{album_name}</field>\n<field name='title'>#{song_title}</field>\n\n<field name='lyrics'>#{song_lyrics}\n</field>\n</add>\n</batch>"
            File.open(local_fname, 'w'){|file| file.write(file_content) }
            puts "[#{Time.now}] Lyrics saved to:  #{local_fname}"
            sleep 2 + rand
          end
        end
       sleep 2 + rand 
      end
    end
  else
    letter_contor++
    continue = false if letters[letter_contor] > "Z"
    sleep 2 + rand
  end
  number += 30
  sleep 2+ rand
end