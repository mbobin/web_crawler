#! /bin/bash

for i in *.txt ; do 
  [[ -f "$i" ]] || continue
  ruby "mod_file.rb" "$i" 
done
