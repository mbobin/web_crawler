fname = ARGV[0]
 
content = File.readlines fname
file_content = "<song>\n"
content.each_with_index{|line, i| 
  file_content += "<artist>#{line.chomp}</artist>\n" if i == 0
  file_content += "<album>#{line.chomp}</album>\n" if i == 1
  file_content += "<title>#{line.chomp}</title>\n" if i == 2
  file_content +=  "<lyrics>\n" if i == 3
  file_content += "#{line}" if i > 3
}
file_content += "</lyrics>\n</song>"

File.open("edited/#{fname}.xml", 'w'){|file| file.write(file_content) }
   puts "[#{Time.now}] Lyrics saved to:  #{fname}"
